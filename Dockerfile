#
# Build stage
#

FROM maven:3.6.0-jdk-8-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

#
# Package stage
#
WORKDIR /home/app/target
EXPOSE 8761

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /home/app/target/eureka-server-0.0.1-SNAPSHOT.jar"]

